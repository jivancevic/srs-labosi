package hr.fer.srs.cipher;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {
	
	private static final int SALT_LENGTH = 16;
	private static final int IV_LENGTH = 16;
	private static final int MAC_LENGTH = 20;
	
	public static void init(String masterPassText) { //throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException, IOException {
		
		try {
			// inicijalni tekst - nulti par adresa-zaporka
			String initialText = "iniTialPass";
			byte[] data = encrypt(masterPassText, initialText);
			
			// zapisavanje sadržaja u passwordManager
			File file = new File("data/passwordManager.txt");
			Files.deleteIfExists(file.toPath());
			file.createNewFile();
			Files.write(file.toPath(), data);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
				
		System.out.println("Password manager initialized.");
	}	
	
	public static void put(String masterPassText, String addressText, String passText) {
		File file = new File("data/passwordManager.txt");
		if (!file.exists()) throw new NullPointerException("Password manager hasn't been initialized.");
		try {
			// čitanje sadržaja 
			byte[] data = Files.readAllBytes(file.toPath());
			
			String plainText = decrypt(masterPassText, data);
			System.out.println(plainText);
			String add = "/".concat(addressText).concat("+").concat(passText);
			Base64.getDecoder().decode(add);
			plainText = plainText.concat(add);
			System.out.println(plainText);
			data = encrypt(masterPassText, plainText);
			
			// zapisavanje sadržaja u passwordManager
			new PrintWriter(file).close();
			Files.write(file.toPath(), data);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Stored password for " + addressText);
	}
	
	public static void get(String masterPassText, String addressText) throws FileNotFoundException {
		String passText = null;
		
		File file = new File("data/passwordManager.txt");
		if (!file.exists()) throw new NullPointerException("Password manager hasn't been initialized.");
		
		try {
			// čitanje sadržaja 
			byte[] data = Files.readAllBytes(file.toPath());
			
			String plainText = decrypt(masterPassText, data);
			System.out.println(plainText);
			String[] pairs = plainText.split("/");
			
			boolean found = false;
			
			for (String s : pairs) {
				if (s.startsWith(addressText.concat("+"))) {
					passText = s.split("+")[1];
					found = true;
					break;
				}
			}
			if (!found) throw new IllegalArgumentException("There is no password saved for that address in passwordManager.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Password for " + addressText + " is: " + passText);
	}
	
	private static byte[] encrypt(String masterPassText, String plainText) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		System.out.println(plainText);
		byte[] bytes = Base64.getDecoder().decode(plainText);
		
		// generiranje random salt-a i inicijalizacijskog vektora
		SecureRandom sr = new SecureRandom();
		byte[] salt = new byte[SALT_LENGTH];
		sr.nextBytes(salt);
		byte[] iv = new byte[IV_LENGTH];
		sr.nextBytes(iv);
					
		// funkcija za derivaciju kljuca na osnovu master passworda i nasumicno generiranog salt-a
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec keySpec = new PBEKeySpec(masterPassText.toCharArray(), salt, 5000, 256);
		SecretKey secretKey = factory.generateSecret(keySpec);
		byte[] key = secretKey.getEncoded();
					
		// podjela jednog 256-bitnog kljuca na 2 128-bitna ključa - 1. za kriptiranje, 2. za MACiranje
		byte[] encriptionKey = Arrays.copyOfRange(key, 0, key.length/2);
		byte[] macKey = Arrays.copyOfRange(key, key.length/2, key.length);
					
		// kriptiranje inicijalnog teksta pomoću ključa za kriptiranje
		SecretKeySpec encriptionSKS = new SecretKeySpec(encriptionKey, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		
		cipher.init(Cipher.ENCRYPT_MODE, encriptionSKS, new IvParameterSpec(iv));
		byte[] cipherResult = cipher.doFinal(bytes);
					
		// MACiranje šifrata i inicijalnog vektora
		Mac mac = Mac.getInstance("HmacSHA1");
		SecretKeySpec macSKS = new SecretKeySpec(macKey, "AES");
		mac.init(macSKS);
		byte[] macResult = mac.doFinal(concat(cipherResult, iv));
					
		// spajanje sadrzaja za zapisivanje u passwordManager
		byte[] result = new byte[salt.length + iv.length + macResult.length + cipherResult.length];
		System.arraycopy(salt, 0, result, 0, salt.length);
		System.arraycopy(iv, 0, result, salt.length, iv.length);
		System.arraycopy(macResult, 0, result, salt.length + iv.length, macResult.length);
		System.arraycopy(cipherResult, 0, result, salt.length + iv.length + macResult.length, cipherResult.length);
		
		return result;
	}
	
	private static String decrypt(String masterPassText, byte[] data) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		
		byte[] salt = Arrays.copyOfRange(data, 0, SALT_LENGTH);
		byte[] iv = Arrays.copyOfRange(data, SALT_LENGTH, SALT_LENGTH + IV_LENGTH);
		byte[] macResult = Arrays.copyOfRange(data, SALT_LENGTH + IV_LENGTH, SALT_LENGTH + IV_LENGTH + MAC_LENGTH);
		byte[] cipherResult = Arrays.copyOfRange(data, SALT_LENGTH + IV_LENGTH + MAC_LENGTH, data.length);
		
		// funkcija za derivaciju kljuca na osnovu master passworda i pročitanog salt-a
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		KeySpec keySpec = new PBEKeySpec(masterPassText.toCharArray(), salt, 5000, 256);
		SecretKey secretKey = factory.generateSecret(keySpec);
		byte[] key = secretKey.getEncoded();
		
		// podjela dobivenog 256-bitnog kljuca na 2 128-bitna ključa - 1. za dekriptiranje, 2. za provjeru MAC-a
		byte[] decriptionKey = Arrays.copyOfRange(key, 0, key.length/2);
		byte[] macKey = Arrays.copyOfRange(key, key.length/2, key.length);
		
		// dekriptiranje pročitanog teksta pomoću ključa za dekripciju
		SecretKeySpec decriptionSKS = new SecretKeySpec(decriptionKey, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, decriptionSKS, new IvParameterSpec(iv));
		byte[] plainResult = cipher.doFinal(cipherResult);
		
		String plainText = Base64.getEncoder().encodeToString(plainResult);
		
		return plainText;
	}
	
	private static byte[] concat(byte[] a, byte[] b) {
		byte[] c = new byte[a.length + b.length];
		System.arraycopy(a, 0, c, 0, a.length);
		System.arraycopy(b, 0, c, a.length, b.length);
		
		return c;
	}
	
	public static void main(String[] args) {
		if (args.length == 0) throw new IllegalArgumentException("You must provide command name as an argument.");
		
		switch (args[0]) {
		case "init": {
			if (args.length != 2) throw new IllegalArgumentException("Function init() can only receive one argument as master password.");
			try {
				init(args[1]); 
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		}
		case "put": {
			if (args.length != 4) throw new IllegalArgumentException("Function put() must receive master password and adress-password pair as arguments.");
			try {
				put(args[1], args[2], args[3]); 
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		}
		case "get": {
			if (args.length != 3) throw new IllegalArgumentException("Function put() must receive master password and adress as arguments.");
			try {
				get(args[1], args[2]); 
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		}
		default: throw new IllegalArgumentException("\"" + args[0] + "\" is not recognized command.");
		}
	}
}
