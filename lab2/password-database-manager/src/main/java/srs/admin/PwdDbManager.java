package srs.admin;

import java.io.Console;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class PwdDbManager {

	private static final int SALT_LENGTH = 16;
	private static char separator = ((char) 29);
	private static String passDatabaseFile = "data/password-database.txt";

	public static boolean authenticateUser(String username, String pass)
			throws Exception {
		String entry = getEntry(username);

		if (entry == null)
			throw new IllegalArgumentException();

		String[] elements = entry.split(String.valueOf(separator));
		String saltString = elements[1];
		String hashString = elements[2];

		byte[] salt = Base64.getDecoder().decode(saltString);
		byte[] hash = Base64.getDecoder().decode(hashString);

		// generiranje testnog hasha
		byte[] testHash = generateHash(pass, salt);

		if (!Arrays.equals(hash, testHash))
			throw new IllegalArgumentException();

		return true;
	}

	public static boolean shouldChangePassword(String username)
			throws Exception {
		String entry = getEntry(username);

		return entry.endsWith("1");
	}

	public static void changePassword(String username) throws Exception {
		changePassword(username, true);
		changeMode(username, false);
	}

	static void addUser(String username) throws Exception {
		if (getEntry(username) != null)
			throw new IllegalArgumentException(
					"User " + username + " already exists.");

		String pass = generatePassword(false);
		String entry = generateEntry(username, pass);

		// dodavanje zapisa u bazu
		List<String> lines = Files.readAllLines(Paths.get(passDatabaseFile));
		lines.add(entry);
		writeToDatabase(lines);
	}

	static void changePassword(String username, boolean isLogin)
			throws Exception {
		String oldEntry = getEntry(username);

		if (oldEntry == null)
			throw new IllegalArgumentException(
					"User " + username + " doesn't exist.");

		String pass = null;
		while (true) {
			pass = generatePassword(isLogin);

			String[] elements = oldEntry.split(String.valueOf(separator));
			String saltString = elements[1];
			String hashString = elements[2];

			byte[] salt = Base64.getDecoder().decode(saltString);
			byte[] hash = Base64.getDecoder().decode(hashString);

			if (!Arrays.equals(hash, generateHash(pass, salt)))
				break;

			System.out.println("Cannot change to same password.");
		}

		String newEntry = generateEntry(username, pass);

		List<String> lines = Files.readAllLines(Paths.get(passDatabaseFile));
		Collections.replaceAll(lines, oldEntry, newEntry);
	}

	static void forcePasswordChange(String username) throws Exception {
		changeMode(username, true);
	}

	static void deleteUser(String username) throws Exception {
		List<String> lines = Files.readAllLines(Paths.get(passDatabaseFile));
		if (!lines.removeIf(
				s -> s.startsWith(username + String.valueOf(separator))))
			throw new NoSuchElementException(
					"User " + username + " doesn't exist.");

		PwdDbManager.writeToDatabase(lines);
	}

	private static String generatePassword(boolean isLogin) throws IOException {
		Console c = System.console();
		String pass = null;
		String repeatedPass = null;

		if (isLogin)
			pass = String.valueOf(c.readPassword("New password: ")).trim();
		else
			pass = String.valueOf(c.readPassword("Password: ")).trim();

		isSafePassword(pass);

		if (isLogin)
			repeatedPass = String
					.valueOf(c.readPassword("Repeat new password: ")).trim();
		else
			repeatedPass = String.valueOf(c.readPassword("Repeat password: "))
					.trim();

		if (!pass.equals(repeatedPass))
			throw new IllegalArgumentException("Password mismatch.");

		return pass;
	}

	private static boolean isSafePassword(String pass) {
		if (pass == null)
			throw new IllegalArgumentException("Password cannot be null");
		else if (pass.length() < 8)
			throw new IllegalArgumentException(
					"Password must be at least 8 characters long.");
		else if (pass.length() > 256)
			throw new IllegalArgumentException(
					"Password must be at most 256 characters long.");
		else if (!isWritable(pass))
			throw new IllegalArgumentException(
					"Password must contain only writable characters.");
		else if (!isStrongPassword(pass))
			throw new IllegalArgumentException(
					"Password must contain at least one (1) character from three (3) of the following categories:\r\n"
							+ "Uppercase letter (A-Z)\r\n"
							+ "Lowercase letter (a-z)\r\n" + "Digit (0-9)\r\n"
							+ "Special character (~`!@#$%^&*()+=_-{}[]\\|:;”’?/<>,.)");

		return true;
	}

	private static boolean isWritable(String pass) {
		char[] chars = pass.toCharArray();

		for (char c : chars)
			if (!(c >= 33 && c <= 126))
				return false;

		return true;
	}

	private static boolean isStrongPassword(String pass) {
		int count = 0;

		Pattern p1 = Pattern.compile("[A-Z]");
		Matcher m1 = p1.matcher(pass);
		if (m1.find())
			count++;

		Pattern p2 = Pattern.compile("[a-z]");
		Matcher m2 = p2.matcher(pass);
		if (m2.find())
			count++;

		Pattern p3 = Pattern.compile("[0-9]");
		Matcher m3 = p3.matcher(pass);
		if (m3.find())
			count++;

		Pattern p4 = Pattern.compile("[^a-zA-Z0-9]");
		Matcher m4 = p4.matcher(pass);
		if (m4.find())
			count++;

		if (count >= 3)
			return true;

		return false;
	}

	private static String generateEntry(String username, String pass)
			throws Exception {
		// generiranje random salta
		SecureRandom random = new SecureRandom();
		byte[] salt = new byte[SALT_LENGTH];
		random.nextBytes(salt);

		// generiranje hasha
		byte[] hash = generateHash(pass, salt);

		// konkateniranje zapisa
		String saltString = Base64.getEncoder().encodeToString(salt);
		String hashString = Base64.getEncoder().encodeToString(hash);

		String mode = "0";
		StringBuilder sb = new StringBuilder();

		sb.append(username).append(separator).append(saltString)
				.append(separator).append(hashString).append(separator)
				.append(mode);

		return sb.toString();
	}

	private static byte[] generateHash(String pass, byte[] salt)
			throws Exception {
		KeySpec keySpec = new PBEKeySpec(pass.toCharArray(), salt, 5000, 128);
		SecretKeyFactory factory = SecretKeyFactory
				.getInstance("PBKDF2WithHmacSHA1");
		return factory.generateSecret(keySpec).getEncoded();
	}

	private static String getEntry(String username) throws Exception {
		List<String> lines = Files.readAllLines(Paths.get(passDatabaseFile));

		for (String line : lines) {
			if (line.startsWith(username + String.valueOf(separator))) {
				return line;
			}
		}

		return null;
	}

	private static void writeToDatabase(List<String> lines) throws Exception {
		FileWriter writer = new FileWriter(passDatabaseFile);

		for (String str : lines) {
			writer.write(str + System.lineSeparator());
		}

		writer.close();
	}

	private static void changeMode(String username, boolean force)
			throws Exception {
		String oldEntry = getEntry(username);

		if (oldEntry == null)
			throw new IllegalArgumentException(
					"User " + username + " doesn't exist.");

		char oldMode = oldEntry.charAt(oldEntry.length() - 1);

		if ((oldMode == '0' && force) || (oldMode == '1' && !force)) {
			String newEntry = oldEntry.substring(0, oldEntry.length() - 1);
			newEntry += force ? "1" : "0";
			List<String> lines = Files
					.readAllLines(Paths.get(passDatabaseFile));
			Collections.replaceAll(lines, oldEntry, newEntry);

			writeToDatabase(lines);
		}
	}

}
