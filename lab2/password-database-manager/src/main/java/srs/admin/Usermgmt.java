package srs.admin;

public class Usermgmt {

	// add - Dodavanje novog korisničkog imena
	private static void add(String username) throws Exception {
		PwdDbManager.addUser(username);

		System.out.println("User " + username + " successfuly added.");
	}

	// passwd - Promjenu lozinke postojećeg korisničkog imena
	private static void passwd(String username) throws Exception {
		PwdDbManager.changePassword(username, false);

		System.out.println("Password change successful.");
	}

	// forcepass - Forsiranje promjene lozinke korisničkog imena
	private static void forcepass(String username) throws Exception {
		PwdDbManager.forcePasswordChange(username);

		System.out.println("User " + username
				+ " will be requested to change password on next login.");
	}

	// del - Uklanjanje postojećeg korisničkog imena
	private static void del(String username) throws Exception {
		PwdDbManager.deleteUser(username);

		System.out.println("User " + username + " successfuly removed.");
	}

	public static void main(String[] args) {
		try {
			if (args.length != 2)
				throw new IllegalArgumentException(
						"Please provide function name and username as arguments.");

			if (System.console() == null)
				System.out.println("null je brate");

			switch (args[0]) {
				case "add" -> {
					try {
						add(args[1]);
					} catch (Exception e) {
						throw new Exception(
								"User add failed. " + e.getMessage());
					}

				}
				case "passwd" -> {
					try {
						passwd(args[1]);
					} catch (Exception e) {
						throw new Exception(
								"Password change failed. " + e.getMessage());
					}
				}
				case "forcepass" -> forcepass(args[1]);
				case "del" -> del(args[1]);
				default -> throw new IllegalArgumentException(
						"\"" + args[0] + "\" is not recognized command.");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
