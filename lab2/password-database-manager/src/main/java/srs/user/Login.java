package srs.user;

import java.io.Console;

import srs.admin.PwdDbManager;

public class Login {

	// Upisivanje korisničkog imena i lozinke pri čemu lozinka ne smije biti
	// vidljiva tijekom upisa
	public static void login(String username) throws Exception {
		Console c = System.console();
		String pass = null;

		long time = 1L;
		while (true) {

			pass = String.valueOf(c.readPassword("Password: ")).trim();

			try {
				if (PwdDbManager.authenticateUser(username, pass))
					break;
			} catch (Exception e) {
				System.out.println("Username or password incorrect.");
				Thread.sleep(time * 1000);
				time *= 2;
			}
		}

		if (PwdDbManager.shouldChangePassword(username)) {
			PwdDbManager.changePassword(username);
		}

		System.out.println("Login successful.");
	}

	public static void main(String[] args) {
		if (args.length != 1)
			throw new IllegalArgumentException(
					"Please provide username as argument.");

		try {
			login(args[0]);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
